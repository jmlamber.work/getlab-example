<?php 
	include_once 'php/functions.php';
	session_start();
	
	//re-direct logged in users
	login_redirect();
?>

<html>
	<head>
		<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
		<meta content="utf-8" http-equiv="encoding">
		
		<!-- Load PHP to Javascript -->
		<script>
			var root = "<?php echo $_SESSION['root'] ?>";
		</script>
		
		<!-- Font Awesome (Icons) -->
		<link rel='stylesheet' href='css/font-awesome-4.5.0/css/font-awesome.css'>
		
		<!-- JQuery -->
		<script src="jquery/jquery-2.1.3.js"></script>
		
		<!-- JQueryUI -->
		<script src="jquery/jquery-ui/jquery-ui.min.js"></script>
		<link rel='stylesheet' href='jquery/jquery-ui/jquery-ui.min.css'>
	
	<style>	
		body {
			padding: 0px;
			margin: 0px;
		}
	
		#desktop {
			position: relative;
			width: 100%;
			height: calc(100vh - 40px);
			overflow: hidden;
			padding: 0px;
			margin: 0px;
			background-color: black;
			background-image: url('wallpaper.jpg');
			background-size: 100% 100%;
		}
		
		.logout {
			top: 0px;
			left: 0px;
			position: absolute;
		}
	</style>
	</head>
	
	<body>
		<!-- Temporary logout button -->
		<div class="logout">
			<a href="php/logout.php"> Logout </a>
		</div>
		
		<!-- Desktop -->
		<div id="desktop">
		
		</div>
	</body>

</html>