<?php
	//Checks if the user is logged in. If so, send the user
	//to his library. If not? To the index with ye.
	
	//basename check for redirect loop issues?
	function login_redirect() {
		//if not from html, then from php
		if(isset($_SESSION['username']) 
			&& !(basename($_SERVER['PHP_SELF']) == "desktop.php") 
			&& !(basename($_SERVER['PHP_SELF']) == "index.php")) {
			header("location: ../desktop.php");
		}
		
		else if(!isset($_SESSION['username']) 
			&& !(basename($_SERVER['PHP_SELF']) == "desktop.php") 
			&& !(basename($_SERVER['PHP_SELF']) == "index.php")) {
			header("location: ../index.php");
		}
		
		//otherwise from html
		else if(isset($_SESSION['username']) && !(basename($_SERVER['PHP_SELF']) == "desktop.php")) {
			header("location: desktop.php");
		}
		
		else if(!isset($_SESSION['username']) && !(basename($_SERVER['PHP_SELF']) == "index.php")) {
			header("location: index.php");
		}
	}
?>