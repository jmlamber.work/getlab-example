//*** TASKBAR START MENU ***//
var ms_startmenu_FIRED = false;

function ms_startmenu_init() {
	ms_startmenu_generateStartMenu();
	
	//Handle startmenu button mouse events
	$('#ms_taskbar_startMenuButton').on('mousedown', function(event) {
		
		//If not visible, bring it up
		if(!$('#ms_taskbar_startMenu').is(':visible')) {
			$('#ms_taskbar_startMenu').show();
			ms_startmenu_FIRED = true;
		}
		
		//Otherwise, just hide it again.
		else
			$('#ms_taskbar_startMenu').hide();
	});
	
	//Prevents startmenu from closing when clicked on
	$('#ms_taskbar_startMenu').on('mousedown', function(event) {
		ms_startmenu_FIRED = true;
	});
	
	//If clicked outside, close startmenu
	$(document).on('mousedown', function(event) {
		if(!ms_startmenu_FIRED)
			$('#ms_taskbar_startMenu').hide();
		else
			ms_startmenu_FIRED = false;
	});
}

//For each app, generate an icon on the start menu
function ms_startmenu_generateStartMenu() {
	
	//for each catagory (iteration method for object properties)
	//catagory is the NAME of the property, not the property itself
	for(var catagory in pluginInfo) {
		if(pluginInfo.hasOwnProperty(catagory)) {
			
			//For catagories with single apps, just put in the app
			if(pluginInfo[catagory].apps.length == 1) {
				var app = pluginInfo[catagory].apps[0];
				
				var ele = document.createElement('li');
				$(ele).addClass('ms_taskbar_startMenuListItem');
				$(ele).addClass(app.pluginName + '_icon');
				$(ele).text(app.appName);
				
				$(ele).click(function(){
					createApp(app.pluginName, root);
					$('#ms_taskbar_startMenu').hide();
				});
				
				$('#ms_taskbar_startMenuList').append(ele);
			}
			
			//For catagories with multiple apps, the apps need to be sub-items
			else {
				
			}
		}
	}
}