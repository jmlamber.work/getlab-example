//*** TASKBAR CLOCK ***//

//System time
function updateTime() {
	
	//Get time
	var currentTime = new Date();
	var hours = currentTime.getHours();
	var minutes = currentTime.getMinutes();
	if(minutes < 10)
		minutes = '0' + minutes;
	var period;
	
	//AM or PM
	if(hours >= 12)
		period = ' PM';
	else
		period = ' AM';
	
	var time = '<p>' + hours + ':' + minutes + period + '</p>';
	
	
	//Then date
	var month = currentTime.getMonth()+1;
	var day = currentTime.getDate();
	var year = currentTime.getFullYear();
	var date = '<p>' + month + '/' + day + '/' + year + '</p>';
	
	//Update our page
	$('#ms_taskbar_clock').html(time + date);
	
	//And force an update every second (clock will be AT MOST one second off)
	setTimeout('updateTime()', 1000);
};