//Stores information regarding dragged items
var ms_dragdrop_active = false;
var ms_dragdrop_content = [];
var ms_dragdrop_position = {};
var ms_dragdrop_icon;

//Starts the dragDrop process
function ms_dragdrop_start(context, icon, process, threshold) {
	context.on('mousedown.dragDrop', function(e) {
		ms_dragdrop_position.x = mouse.x;
		ms_dragdrop_position.y = mouse.y;
		
		$(document).on('mousemove.dragDrop', function(e) {
			//Start dragging after a small threshold
			if(!ms_dragdrop_inRadius(ms_dragdrop_position, mouse, 10)) {
				
				//Run the passed in process
				if(process)
					process(e, context);
				
				if(!ms_dragdrop_active) {
					
					//If an icon was passed in, generate a div for it
					if(icon) {						
						//create our drag icon
						ms_dragdrop_icon = document.createElement('div');
						$(ms_dragdrop_icon).addClass(icon);
						$(ms_dragdrop_icon).addClass('ms_dragDropIcon');
						
						var dragDropCount = document.createElement('div');
						$(dragDropCount).addClass('ms_dragDropCount');
						$(dragDropCount).text(ms_dragdrop_content.length);
						$(ms_dragdrop_icon).append(dragDropCount);
						
						//Have it follow the mouse
						$(ms_dragdrop_icon).css({'left': mouse.x+5, 'top': mouse.y+5});
						$('body').on('mousemove.dragDrop_icon', function() {
							$(ms_dragdrop_icon).css({'left': mouse.x+5, 'top': mouse.y+5});
						});
						
						$('body').append(ms_dragdrop_icon);
					}
					
					ms_dragdrop_active = true;
				}
			}
		});
	});
}

//Ends the dragDrop process
function ms_dragdrop_stop(context, process) {
	context.on('mouseup.dragDrop', function(e) {
		
		//destroy dragDrop div if exists
		$(ms_dragdrop_icon).remove();
		$('body').off('mousemove.dragDrop_icon');
		
		//Destroy event handlers
		$(document).off('mousemove.dragDrop');
		
		//Run specified process
		if(process && ms_dragdrop_active)
			process(e, context);
		
		ms_dragdrop_active = false;
	});
}
ms_dragdrop_stop($(document), null);

//If A is within a specified distance from B
function ms_dragdrop_inRadius(posA, posB, distance) {
	var dist = Math.sqrt(Math.pow(posA.x - posB.x, 2) + Math.pow(posA.y - posB.y, 2));
	
	if(dist <= distance)
		return true;
	
	return false;
}