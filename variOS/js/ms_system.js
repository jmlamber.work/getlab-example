//*** GLOBAL FUNCTIONS ***//
//Counters
var idCounter = 0;
var zIndexCounter = 0;

//States
var activeMousedown = false;
var taskbarTaskpanelSubmenuMousedown = false;
var taskbarTaskpanelSubmenuClick = false;

//Stores information regarding currently available applications
var pluginInfo = {};

//Refreshes each application (if needed) and the taskbar
function globalRefresh() {
	for(var i = 0; i < taskbar.length; i++) {
		for(var j = 0; j < taskbar[i].apps.length; j++) {
			taskbar[i].apps[j].refresh();
		}
	}
	
	refreshTaskbar();
}

function addPluginInfo(displayName, pluginName, formats, catagory, catagoryName) {
	
	//Our apps info
	var info = {
		appName: displayName,
		pluginName: pluginName,
		formats: formats,
	}
	
	//If the catagory exists, add to it
	if(pluginInfo[catagory]) {
		pluginInfo[catagory].apps.push(info);
	}
	
	//Otherwise, create it
	else {
		pluginInfo[catagory] = {};
		pluginInfo[catagory].catagoryName = catagoryName;
		
		pluginInfo[catagory].apps = [];
		pluginInfo[catagory].apps.push(info);
	}
}



//Sets the active window
function setActive(window, clicked) {
	
	if(!activeMousedown) {
		//Set everything as inactive
		for(var i = 0; i < taskbar.length; i++) {
			for(var j = 0; j < taskbar[i].apps.length; j++) {
				taskbar[i].apps[j].getWindow().removeClass('ms_window_active');
				taskbar[i].apps[j].getWindow().addClass('ms_window_inactive');
			}
		}
		
		//if a window is specified, set it as active
		if(window) {
			$(window).removeClass('ms_window_inactive');
			$(window).addClass('ms_window_active');
			$(window).zIndex(nextZIndex());
			
			//Prevent re-activation on document level
			if(clicked)
				activeMousedown = true;
		}
		
	}
	
	else
		activeMousedown = !activeMousedown;
}
$(window).mousedown(function() {
	setActive();
});


//Sets the next z-index
function nextZIndex() {
	zIndexCounter += 1;
	
	//For items which should ALWAYS be above, increment their z-index
	$('#ms_taskbar').zIndex(zIndexCounter + 1);
	$('#ms_taskbar_startMenu').zIndex(zIndexCounter + 1);
	
	return zIndexCounter;
}