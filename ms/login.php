<?php
	ini_set('display_errors', 'On');
	error_reporting(E_ALL | E_STRICT);

	include_once 'php/functions.php';
	include_once 'php/ChromePhp.php'; 

	//start our new session
	session_start();
	
	if(isset($_POST['username']) && isset($_POST['h'])) {
		$datapath = "/var/www/html/ms/data/user_data";
		$username = strtolower($_POST['username']);
		$hash = $_POST['h'];
		$userpath = $datapath . '/' . $hash;
		$root = '/root';
		
		//store session values
		$_SESSION['hash'] = $hash;
		$_SESSION['username'] = $username;
		$_SESSION['userpath'] = $userpath;
		$_SESSION['root'] = $root;
		
		//check if username already exists
		if(!file_exists($userpath)) {
			$old = umask(0);
			
			mkdir($userpath, 0777, true);
			mkdir($userpath . $root, 0777, true);
			mkdir($userpath . $root . '/' . "audio", 0777);
			mkdir($userpath . $root . '/' . "video", 0777);
			mkdir($userpath . $root . '/' . "images", 0777);
			
			umask($old);
		}
		else {
			echo "File exists!";
		}
	}
	
	//finally, log in.
	login_redirect();
?>