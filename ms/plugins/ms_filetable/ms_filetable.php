<?php
	session_start();
	
	$option = $_POST['method'];

	if($option == 'pull') {
		function formatSizeUnits($bytes) {
			if ($bytes >= 1073741824)
				$bytes = number_format($bytes / 1073741824, 2) . 'GB';
			elseif ($bytes >= 1048576)
				$bytes = number_format($bytes / 1048576, 2) . 'MB';
			elseif ($bytes >= 1024)
				$bytes = number_format($bytes / 1024, 2) . 'KB';
			elseif ($bytes > 1)
				$bytes = $bytes . 'B';
			else
				$bytes = '0B';
			return $bytes;
		}

		$dir = $_POST['directory'];
		$realDir = $_SESSION['userpath'] . $dir;
		$files = scandir($realDir);
		
		//Build our result array
		$result = array();
		for ($i = 0; $i < count($files); $i++) {
			$fileInfo = pathinfo($realDir . '/' . $files[$i]);
			
			$result[$i]['name'] = $files[$i];
			$result[$i]['type'] = filetype($realDir . '/' . $files[$i]);
			$result[$i]['time'] = date("m/d/y H:m", filemtime($realDir . '/' . $files[$i]));
			$result[$i]['size'] = formatSizeUnits(filesize($realDir . '/' . $files[$i]));
			
			$result[$i]['extension'] = $fileInfo['extension'];
			$result[$i]['path'] = $dir . '/' . $files[$i];
		}
		
		//Remove current directory
		array_shift($result);
		
		//If at base directory, remove parent directory
		if($_POST['directory'] == $_SESSION['root'])
			array_shift($result);
		
		//For easy viewing, sort it by directory, then by additional sort methods
		//Default should be alpha decending
		
		echo json_encode($result);
	}
	
	else if($option == 'move') {
		$dir = $_POST['directory'];
		$realDir = $_SESSION['userpath'] . $dir;
		$userpath = $_SESSION['userpath'];
		$selected = json_decode($_POST['selected']);
		
		for($i = 0; $i < count($selected); $i++) {
			$file = $userpath . $selected[$i]->{'path'};
			$destination = $realDir . '/' . $selected[$i]->{'name'};
			rename($file, $destination);
		}
		
		echo json_encode(error_get_last());
	}
?>