//Filetable allows the user to browse shown directorys


var	displayName = 'File Browser';
var	pluginName = 'ms_filetable';
var	formats = 'file';
var catagory = 'filebrowser';
var catagoryName = 'File Browser';

addPluginInfo(displayName, pluginName, formats, catagory, catagoryName);

(function($) {
	$.fn.ms_filetable = function(options) {
		var _this = this; //For accessing within various scopes
		var settings; //Passed in settings			
		var selectable; //our selectable plugin object
		var directory; //The currently displayed directory
		var id; //element id
		var window;	//Window object which contains the app content
		
		var columns = [
			{idName: 'name', displayName: 'Name', visible: true},
			{idName: 'type', displayName: 'Type', visible: true},
			{idName: 'time', displayName: 'Date Modified', visible: true},
			{idName: 'size', displayName: 'Size', visible: true},
			{idName: 'extension', displayName: 'Extension', visible: false},
			{idName: 'path', displayName: 'Path', visible: false}
		];
		
		this.init = function() {
			settings = $.extend(options);

			//set default directory
			if(settings.appOptions)
				directory = settings.appOptions;
			else
				directory = root;
			
			//set id
			_this.setID(settings.id);
			
			//Set parent div type
			_this.addClass('ms_filetable_container');
			
			//Refresh (re-create/create) our table
			_this.refresh(directory);
		}
		
		// ---- functions ---- //
		//AJAX method for getting directory data
		this.refresh = function(directory_i) {
			if(!directory_i)
				directory_i = directory;
			
			$.ajax({
				method: 'POST',
				url: ms_filetable_php,
				data: {directory: directory_i, method: 'pull'},
				dataType: 'json',
				success: function(data) {
					directory = directory_i;
					
					//Build table
					populate(data);
				
					//Enable sorting
					$(id + " > table").tablesorter();
					
					//Enable selecting					
					selectable = $(id + " > table").ms_selectable({columns: columns});
					
					//Enable resizing
					$(id + " > table").colResizable({
						liveDrag: true
					});
					
					//Enable double-click (only need consider one item)
					$(id + " > table > tbody > tr").dblclick(function() {
						var selected = selectable.selected()[0];
						
						//If type is a directory, switch to it
						if(selected.type == 'dir')
							changeDirectory(selectable.selected()[0]);
						
						//Otherwise, open its respective app
					});
					
					//Enable dragging on files
					ms_dragdrop_start($(id + " > table > tbody > tr"), 'ms_filetable_icon', rowPickupFile, 5);
					ms_dragdrop_stop($(id + " > table > tbody > tr"), rowDropFile); //drop event on table rows
					ms_dragdrop_stop($(id + " > table"), null); //prevent access to anything else on the table
					ms_dragdrop_stop($(id), tableDropFile); //drop event on remainder of body
					
					//Set our window header
					window.setHeader(directory);
					
					//refresh the taskbar to reflect our new header?
					refreshTaskbar();
				}
			});
		} 
		
		function populate(data) {
			var table = "";
			
			//First, generate the columns
			var thead = "<thead class='ms_filetable'>" + "<tr class='ms_filetable'>"
			for(var i = 0; i < columns.length; i++) {
				thead += "<th class='ms_filetable'>" + columns[i].displayName + '</th>';
			}
			thead += '</tr>' + '</thead>';
			
			
			//Populate our table with rows
			var tbody = "<tbody class='ms_filetable'>";
			for(var i = 0; i < data.length; i++) {
				var tr = "<tr class='ms_filetable_row'>";
				
				for(var j = 0; j < columns.length; j++) {
					tr += "<td class='ms_filetable'>" + data[i][columns[j].idName] + '</td>';
				}
				
				tr += '</tr>';
				tbody += tr;
			}
			tbody += '</tbody>';
			
			table = "<table class='ms_filetable'>" + thead + tbody + "</table>";
			
			//update the element with our new table
			$(id).html(table);
			
			//Hide inactive columns
			for(var i = 0; i < columns.length; i++) {
				if(columns[i].visible == false) {
					$(id + ' td:nth-child(' + (i+1) + '), ' + id + ' th:nth-child(' + (i+1) + ')').hide();
				}
				else {
					$(id + ' td:nth-child(' + (i+1) + '), ' + id + ' th:nth-child(' + (i+1) + ')').show();
				}
			}
		};
		
		function rowPickupFile(e, context) {
			ms_dragdrop_content = selectable.selected();
		}
		
		//Move to file location IF location is a dir
		function rowDropFile(e, context) {
			
			//Get the dragged to row
			var row = $(e.target).parent('tr').get(0);
			
			//Get path to send to
			var i;
			for(i = 0; i < columns.length; i++)
				if(columns[i].idName == 'path')
					break;
			
			var path = $(row.cells[i]).text();
			
			$.ajax({
				method: 'POST',
				url: ms_filetable_php,
				data: {directory: path, selected: JSON.stringify(ms_dragdrop_content), method: 'move'},
				dataType: 'json',
				success: function(data) {
					globalRefresh();
				}
			});
		}
		
		//Move to current location
		function tableDropFile(e, context) {
			
			$.ajax({
				method: 'POST',
				url: ms_filetable_php,
				data: {directory: directory, selected: JSON.stringify(ms_dragdrop_content), method: 'move'},
				dataType: 'json',
				success: function(data) {
					globalRefresh();
				}
			});
		}
		
		function changeDirectory(dir) {			
			//If we're heading back, rather than just endlessly extend the str
			// cut the unnessary parts out
			if(dir.name == '..') {
				dir.path = dir.path.substring(0, dir.path.lastIndexOf('/'));
				dir.path = dir.path.substring(0, dir.path.lastIndexOf('/'));
			}
			
			_this.refresh(dir.path);
		}
		
		//saves and exits the app
		this.close = function(){
			//nothing important here!
		};
		
		this.setID = function(idNum) {
			id = '#a' + idNum;
			_this.attr('id', 'a' + idNum);
		};
		
		this.setWindow = function(newWindow) {
			window = newWindow;
		};
			
		//Quick method for accessing selected items
		this.getSelected = function() {
			return selected;
		};
		
		this.getID = function() {
			return id;
		}
		
		this.getWindow = function() {
			return window;
		}

		return this;
	};
}(jQuery));