(function($) {
	$.fn.ms_draggable = function(options) {
		var settings = $.extend(options);
		var _this = this;
		var handle = $(settings.handle);
		var container = $(settings.container);
		var drag = settings.drag;
		var x;
		var y;
		
		//If no handle is specified, use this
		if(handle == null)
			handle = _this;
		
		//Bind the start of our movement to the handle
		handle.on('mousedown', mousedown);
		
		function mouseup() {
			$(window).off('mousemove', mousemove);
			$(window).off('mouseup', mouseup);
		}
		
		//Due to return false, the event isn't propagating
		function mousedown(event) {
			$(window).mousemove(mousemove);
			$(window).mouseup(mouseup);
			x = mouse.x;
			y = mouse.y;
			event.preventDefault();
		}
		
		function mousemove() {			
			var newX = _this.position().left - (x - mouse.x);
			var newY = _this.position().top - (y - mouse.y);
			x = mouse.x;
			y = mouse.y;
			
			if(x < 0 || x > container.width())
				newX = _this.position().left;
			if(y < 0 || y > container.height())
				newY = _this.position().top;
			
			_this.css({left: newX, top: newY});
			
			drag();
		}
	};
}(jQuery));