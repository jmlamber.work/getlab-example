//Selectable plugin ment for use in basic html tables

(function($) {	
	$.fn.ms_selectable = function(options) {
		var settings = $.extend(options);
		var _this = this;
		var columns = settings.columns;
		var lastSelectedIndex = -1; //Last row selected
		var selected = []; //currently selected items
		var ctrlDown = false; //check when ctrl was last hit
		
		$(this.selector + ' > tbody > tr').mousedown(function(event) {
			var selectedIndex = $(this).index();
			
			//shift-mousedown (select all in range)
			if(event.shiftKey && lastSelectedIndex > -1) {
				var start = Math.min(selectedIndex, lastSelectedIndex);
				var end = Math.max(selectedIndex, lastSelectedIndex);
				
				//Note: nth-index starts at 1, not 0. Blame CSS.
				for(var i = start+1; i < end+2; i++) {
					$(_this.selector + '> tbody > tr:nth-child(' + i + ')').addClass('selected');
				}
			}
			
			//ctrl-mousedown (toggle select multiple)
			else if(event.ctrlKey && selected.length <= 1) {
				ctrlDown = true;
				if($(this).hasClass('selected'))
					$(this).removeClass('selected');
				else
					$(this).addClass('selected');
			}
			
			//mousedown (select only current, ignore if multiple items already selected (for drag purposes))
			else if(selected.length <= 1) {
				$(this).addClass('selected');
				$(this).siblings().removeClass('selected');
			}

			//Update our last selected index
			lastSelectedIndex = selectedIndex;
			
			//Update our list of selected items
			var selected_row = $(_this).find('.selected');
			selected = [];
			
			for (var i = 0; i < selected_row.length; i++) {
				var row = {};
				for (var j = 0; j < selected_row[i].cells.length; j++)
					row[columns[j].idName] = $(selected_row[i].cells[j]).text();
				selected.push(row);
			}
		});
		
		$(this.selector + ' > tbody > tr').click(function(event) {
			var selectedIndex = $(this).index();
			
			if(event.shiftKey) {
				//do nothing
			}
			
			else if(event.ctrlKey && selected.length > 1) {
				
				if(!ctrlDown) {
					if($(this).hasClass('selected'))
						$(this).removeClass('selected');
					else
						$(this).addClass('selected');	
				}
				ctrlDown = false;
			}
			
			else if(selected.length > 1) {
				$(this).addClass('selected');
				$(this).siblings().removeClass('selected');
			}
			
			//Update our last selected index
			lastSelectedIndex = selectedIndex;
			
			//Update our list of selected items
			var selected_row = $(_this).find('.selected');
			selected = [];
			
			for (var i = 0; i < selected_row.length; i++) {
				var row = {};
				for (var j = 0; j < selected_row[i].cells.length; j++)
					row[columns[j].idName] = $(selected_row[i].cells[j]).text();
				selected.push(row);
			}
		});
		
		this.selected = function() {
			return selected;
		}
		
		return this;
	};
}(jQuery));