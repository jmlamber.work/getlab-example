//IMPORTANT GOAL: Either remove the use of 'selector' completely, or enforce a rule in which to use them by.

(function($) {
	$.fn.ms_window = function(options) {
		var _this = this;
		var settings;
		var id;
		var container;
		var app;
		
		var maximizeBorderStart = 5; //px
		var maximizeBorderEnd = 10; //px
		
		//States
		var minimized;
		var maximized;
		var maximizeTopEffect;
		var maximizeLeftEffect;
		var maximizeRightEffect;
		
		//Pre-maximized attributes
		var width;
		var height;
		var x;
		var y;
		
		_this.init = function() {
			settings = $.extend(options);
			container = $('#desktop');
			_this.setID(settings.id);

			maximizeBorderStart = 5; //px
			maximizeBorderEnd = 10; //px
			
			//States
			minimized = false;
			maximized = false;
			maximizeTopEffect = false;
			maximizeLeftEffect = false;
			maximizeRightEffect = false;		

			_this.addClass('ms_window');
			_this.append(
				"<div class='ms_window_header'>" + 
					"<div class='ms_window_dragbar'>" + 
						"<div class='ms_window_descriptor'></div>" + 
					"</div>" + 
					"<div class='ms_window_buttons'>" + 
						"<button class='ms_window_max'> + </button>" + 
						"<button class='ms_window_min'> - </button>" + 
						"<button class='ms_window_close'> x </button>" + 
					"</div>" + 
				"</div>" + 
				"<div class='ms_window_body'></div>"
			);
			
			//enable resize
			resizable();
		
			_this.ms_draggable({
				handle: '#' + _this.attr('id') + ' .ms_window_dragbar',
				container: '#' + container.attr('id'),
				drag: function() {
					
					//Maximize if brought to top
					if(!maximized && mouse.y - maximizeBorderStart <= 0 
					&& mouse.y + maximizeBorderStart >= 0
					&& mouse.x > maximizeBorderStart 
					&& mouse.x < container.width() - maximizeBorderStart) {
						
						//Mouseup event actually maximizes if mouse is released in trigger area
						container.mouseup(dragMouseupTop);
						
						//Display maximization effect if not already there
						if(!maximizeTopEffect) {
							var ele = document.createElement('div');
							ele.className += ' ms_window_maximizeTop';
							container.append(ele);
							maximizeTopEffect = true;
						}
					}
					
					//Cancel maximizeTop effect if not near the top of the screen
					else if(!maximized && Math.abs(mouse.y) - maximizeBorderEnd > 0 
						&& mouse.x > maximizeBorderStart 
						&& mouse.x < container.width() - maximizeBorderStart) {
						
						if(maximizeTopEffect) {
							$(container.selector + ' > .ms_window_maximizeTop').remove();
							maximizeTopEffect = false;
						}
					}
					
					//Cancel maximization if dragged while maximized
					if(maximized) {
						_this.width(width);
						_this.height(height);
						y = 0;
						
						//For our x value, center the window on the mouse as much as
						//	possible without actually going out of the screen.
						x = mouse.x - width/2;
						if(x + width > container.width())
							x = container.width() - width;
						else if(x < 0)
							x = 0;
						
						_this.css({left: x, top: y});
						
						_this.removeClass('ms_window_maximized');
						
						resizable();
						
						maximized = false;
					}
					
					//Half maximize to the left
					
					//Half maximize to the right
				}
			});
				
			//Maximize/minimize/close
			$(id + ' .ms_window_max').click(function() {
				_this.maximize();
			});
			
			$(id + ' .ms_window_min').click(function() {
				_this.minimize();
			});
			
			$(id + ' .ms_window_close').click(function() {
				_this.close();
			});
			
			//Make active if clicked
			$(_this).children().mousedown(function() {
				setActive(_this, true);
			});
			setActive(_this, false);
			
			//Set initial z-index
			$(_this).zIndex(nextZIndex());	
		};
		
		
		
		//*** PRIVATE FUNCTIONS ***//
		
		//Event for dragging a window to the top of the screen (to maximize) 
		var dragMouseupTop = function() {
			
			//If dropped, maximize and remove effect
			//(check had to be done here for some wierd reason)
			if(!maximized && mouse.y-maximizeBorderEnd <= 0 && mouse.y+maximizeBorderEnd >= 0) {
				_this.maximize();
				
				//remove effect
				$(container.selector + ' > .ms_window_maximizeTop').remove();
				maximizeTopEffect = false;
			}
			
			container.unbind('mouseup', dragMouseupTop);
		}
		
		function resizable() {
			_this.resizable({
				handles: "all"
			});
		}
		
		function unresizable() {
			_this.resizable('destroy');
		}
		
		//*** PUBLIC FUNCTIONS ***//
		
		//Returns the jquery object refering to the windows body
		_this.body = function() {
			return $(id + " .ms_window_body");
		};
		
		//Minimizes the window
		_this.minimize = function() {			
			if(minimized == false) {
				_this.hide();
				minimized = true;
			}
			else {
				_this.show();
				minimized = false;
			}
		};
		
		//Maximizes the window (update with css/drag later)
		_this.maximize = function() {
			if(maximized == false) {
				//store current demensions
				width = _this.width();
				height = _this.height();
				x = _this.position().left;
				y = _this.position().top;
				
				//maximize the window
				_this.width('100%');
				_this.height('100%');
				_this.css({left: 0, top: 0});
				
				//Add a class for styles
				_this.addClass('ms_window_maximized');
				
				//When maximized, we don't want to allow resizing
				unresizable();
				
				maximized = true;
			}
			else {
				//Revert to old size
				_this.width(width);
				_this.height(height);
				_this.css({left: x, top: y});
				
				//Remove maximized class
				_this.removeClass('ms_window_maximized');
				
				//Re-enable resizing
				resizable();
				
				maximized = false;
			}
		};
		
		//Exits the application
		_this.close = function() {
			
			//Safely close the application
			app.close();
			
			//The mercilessly kill it (system)
			killApp(app);
		};
		
		//Add menu/functionality to taskbar icon
		this.taskbar = function(ele, subitem) {
			
			$(ele).click(function() {	
				
				//If minimized, bring up
				if(minimized) {
					_this.minimize();
					setActive(_this, false);
				}
				
				//If not active, make active
				else if(!minimized && !_this.isActive()) {
					setActive(_this, false);
				}
					
				//If active and not minimized, minimize
				else
					_this.minimize();
			});
			
			//Prevents event propogation from de-activating an app when its
			//	respective taskbar icon is clicked
			$(ele).mousedown(function() {
				activeMousedown = true;
			});
			
			//If the element is a subitem, close upon hitting the 'x' button
			if(subitem) {
				$(ele).children('.ms_taskbar_taskpanelSubmenuItemButton').click(function() {
					_this.close();
				});
			}
		};
		
		_this.isActive = function() {
			return _this.hasClass('ms_window_active');
		}
		
		_this.setHeader = function(headerInfo) {
			$(id + ' > .ms_window_header > .ms_window_dragbar > .ms_window_descriptor').html(headerInfo);
		};
		
		_this.setID = function(idNum) {
			id = '#w' + idNum;
			_this.attr('id', 'w' + idNum);
		};
		
		_this.getID = function() {
			return id;
		}
		
		_this.setApp = function(newApp) {
			app = newApp;
		};

		_this.getHeader = function() {
			return $(id + ' > .ms_window_header > .ms_window_dragbar > .ms_window_descriptor').html();
		}

		_this.init();
		return _this;
	};
}(jQuery));