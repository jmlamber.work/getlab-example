//Generates a context menu

//NOTE::Apparently, contextmenu fires after both mousedown and mouseup, rather than at the same time as mouseup. Therefore, mouseup will propogate down to document before we have to worry about the contextmenu events, making destroying old contextmenus easy.

//state for managing event handlers
var ms_contextmenu_fired = false;
$(document).on('contextmenu', function(e) {
	e.preventDefault();
	ms_contextmenu_fired = false;
});

(function($) {
	$.fn.ms_contextmenu = function(options) {
		var _this = this;
		var settings = $.extend(options);
		var generate_list = settings.list;
		var contextmenu;
		
		var itemWidth = 260;
		var itemHeight = 25;
		var dividerHeight = 1;
		var borderHeight = 1;
		var borderWidth = 1;
		
		$(this).on('contextmenu', function(event) {
			console.log(this);
			console.log(generate_list);
			if(!ms_contextmenu_fired) {
				ms_contextmenu_fired = true;
				
				//prevent the default context menu from popping up
				event.preventDefault(); 
				
				//create a new context menu
				if(generate_list)
					generate_contextmenu();
			}
		});
		
		function generate_contextmenu() {
			var list = generate_list();
			
			//create the actual DOM elements
			contextmenu = create_menu(list, 'body', mouse.x, mouse.y, false);
			
			//If a mousedown has occured outside of our menu, close it.
			$(document).on('mousedown', mouseaction_off);
			$(document).on('mouseup', mouseaction_off);
		}
		
		//object, int, int, boolean
		function create_menu(list, parent, x, y, submenu) {
			//our menu element
			var menu = document.createElement('div');
			$(menu).addClass('ms_contextmenu');
			$(parent).append(menu);
			
			if(submenu)
				$(menu).addClass('ms_contextmenu_submenu');			
			
			//set its position
			set_position(menu, x, y, itemWidth, menu_height(list), submenu);
			
			//for each catagory
			for(var i = 0; i < list.length; i++) {
				
				//create each item
				for(var j = 0; j < list[i].length; j++) {
					var item = document.createElement('div');
					$(item).addClass('ms_contextmenu_item');
					
					//add it to the menu
					$(menu).append(item);
					
					//add the items icon
					var icon = document.createElement('div');
					$(icon).addClass('ms_contextmenu_itemIcon');
					if(list[i][j].icon)
						$(icon).addClass(list[i][j].icon);
					$(item).append(icon);
					
					//text
					var text = document.createElement('div');
					$(text).addClass('ms_contextmenu_itemText');
					if(list[i][j].text)
						$(text).text(list[i][j].text);
					$(item).append(text);
					
					//hint
					var hint = document.createElement('div');
					$(hint).addClass('ms_contextmenu_itemHint');
					if(list[i][j].hint)
						$(hint).text(list[i][j].hint);
					$(item).append(hint);
					
					//callback
					if(list[i][j].call) {
						var callback = list[i][j].call;
						
						$(item).on('click', function() {
							callback();
							close();
						});	
					}
					
					//and submenu
					if(list[i][j].submenu) {
						var offset = $(item).offset();
						
						var submenu = create_menu(list[i][j].submenu, menu, offset.left, offset.top, true);
						$(item).append(submenu);
						
						//Display on hover only
						$(item).hover(
							function() {
								$(submenu).show();
							},
							function() {
								$(submenu).hide();
							}
						);
						
						//Icon which informs the user of a submenu
						var submenuIcon = document.createElement('div');
						$(submenuIcon).addClass('ms_contextmenu_submenuIcon');
						$(submenuIcon).addClass('fa fa-caret-right');
						$(item).append(submenuIcon);
					}
				}
				
				//Between catagories, add a divider
				var divider = document.createElement('div');
				$(divider).addClass('ms_contextmenu_divider');
				$(menu).append(divider);
			}

			return menu;
		}
		
		//Sets the position of our menu. Default is bottom-right relative to the given coords.
		function set_position(menu, x, y, width, height, submenu) {
			
			//If its a normal menu, position corner at mouse
			if(!submenu) {
				//determine X to be left or right of the mouse
				if(x + width < $(document).width())
					$(menu).css({'left':x});
				else
					$(menu).css({'left':x-width});
				
				//determine Y to be top or bottom of the mouse
				if(y + height < $(document).height())
					$(menu).css({'top':y});
				else
					$(menu).css({'top':y-height});
			}
			
			//Otherwise, position relative to parent ele
			else {				
				//Determine X to be left or right of its parent
				if(x + width*2 < $(document).width())
					$(menu).css({'left':width});
				else
					$(menu).css({'left':-width});
				
				//Determine Y to be above or below its parent
				if(y + height < $(document).height())
					$(menu).css({'top':height});
				else
					$(menu).css({'top':itemHeight});
			}
		}
		
		function menu_height(list) {
			var height = 0;
			
			for(var i = 0; i < list.length; i++) {
				//menu items
				height += list[i].length*itemHeight;
				
				//dividers
				if(i > 0)
					height += dividerHeight;
			}			
			
			height += borderHeight*2;
			return height;
		}
		
		function mouseaction_off(event) {			
			if(!ms_contextmenu_fired) {
				console.log('wha?');
				if(! $(event.target).parents('.ms_contextmenu').length > 0) {
					console.log(true);
					close();
				}
			}
		}
		
		function close() {
			console.log('closed');
			
			//destroy our menu
			$(contextmenu).remove();
			
			//And all related handlers
			$(document).off('mousedown', mouseaction_off);
			$(document).off('mouseup', mouseaction_off);
		}
	};
}(jQuery));