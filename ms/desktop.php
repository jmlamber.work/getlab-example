<?php 
	include_once 'php/functions.php';
	session_start();
	
	//re-direct logged in users
	login_redirect();
?>

<html>
	<head>
		<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
		<meta content="utf-8" http-equiv="encoding">
		
		<!-- Load PHP to Javascript -->
		<script>
			var root = "<?php echo $_SESSION['root'] ?>";
		</script>
		
		<!-- Font Awesome (Icons) -->
		<link rel='stylesheet' href='css/font-awesome-4.5.0/css/font-awesome.css'>
		
		<!-- JQuery -->
		<script src="jquery/jquery-2.1.3.js"></script>
		
		<!-- JQueryUI -->
		<script src="jquery/jquery-ui/jquery-ui.min.js"></script>
		<link rel='stylesheet' href='jquery/jquery-ui/jquery-ui.min.css'>
		
		<!-- colResizable -->
		<script src='plugins/colResizable/colResizable-1.5.min.js'></script>
		
		<!-- tablesorter -->
		<script src='plugins/tablesorter/jquery.tablesorter.js'></script>
		<link rel='stylesheet' href='plugins/tablesorter/blue/style.css'>
		
		<!-- ms_system -->
		<script src='js/ms_system.js'></script>
		<link rel='stylesheet' href='css/ms_system.css'>
		
		<!-- ms_desktop -->
		<script src='js/ms_desktop.js'></script>
		
		<!-- --- --- --- --- --->
		<!-- CUSTOM SCRIPTS --->
		<!-- --- --- --- --- --->
			
		<!-- ms_mouse -->
		<script src='js/ms_mouse.js'></script>
		
		<!-- ms_dragdrop -->
		<script src='js/ms_dragdrop.js'></script>
		
		<!-- ms_startmenu -->
		<script src='js/ms_taskbar/ms_startmenu.js'></script>
		
		<!-- ms_taskpanel -->
		<script src='js/ms_taskbar/ms_taskpanel.js'></script>
		
		<!-- ms_clock -->
		<script src='js/ms_taskbar/ms_clock.js'></script>
		
		<!-- --- --- --- --- --->
		<!-- CUSTOM PLUGINS --->
		<!-- --- --- --- --- --->
		
		<!-- ms_contextmenu -->
		<script src='plugins/ms_contextmenu/ms_contextmenu.js'></script>
		<link rel='stylesheet' href='plugins/ms_contextmenu/ms_contextmenu.css'>
		
		<!-- ms_window -->
		<script src='plugins/ms_window/ms_window.js'></script>
		<link rel='stylesheet' href='plugins/ms_window/ms_window.css'>
		
		<!-- ms_filetable -->
		<script> var ms_filetable_php='plugins/ms_filetable/ms_filetable.php' </script>
		<script src='plugins/ms_filetable/ms_filetable.js'></script>
		<link rel='stylesheet' href='plugins/ms_filetable/ms_filetable.css'>

		<!-- ms_selectable -->
		<script src='plugins/ms_selectable/ms_selectable.js'></script>
		
		<!-- ms_draggable -->
		<script src='plugins/ms_draggable/ms_draggable.js'></script>
		
		<!-- ms_taskbar -->
		<link rel='stylesheet' href='css/ms_taskbar.css'>

	
	<style>	
		body {
			padding: 0px;
			margin: 0px;
		}
	
		#desktop {
			position: relative;
			width: 100%;
			height: calc(100vh - 40px);
			overflow: hidden;
			padding: 0px;
			margin: 0px;
			background-color: black;
			background-image: url('wallpaper.jpg');
			background-size: 100% 100%;
		}
	</style>
	</head>
	
	<body>
		<div id='desktop'></div>
		<div id='ms_taskbar'>
			<div id='ms_taskbar_startMenuButton'></div>
			<div id='ms_taskbar_startMenu'>
				<div id='ms_taskbar_startMenuUserInfo'>
					Username: <?php echo $_SESSION['username'] ?>
					<a href="php/logout.php"> Logout </a>
				</div>
				<ul id='ms_taskbar_startMenuList'></ul>
			</div>
			<div id='ms_taskbar_taskpanel'></div>
			<div id='ms_taskbar_clock'></div>
		</div>
		
		<script>
			init();
			
			var filetablePinned = {
				pluginName: 'ms_filetable',
				pinned: true,
				apps: []
			};
			taskbar.push(filetablePinned);
								
			refreshTaskbar();

		</script>
	</body>

</html>