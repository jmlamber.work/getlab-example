<?php
	session_start();
?>

<html>
	<head>
		<!-- JQuery -->
		<script src="jquery/jquery-2.1.3.js"></script>
		
		<!-- JQueryUI -->
		<script src="jquery/jquery-ui/jquery-ui.min.js"></script>
		<link rel='stylesheet' href='jquery/jquery-ui/jquery-ui.min.css'>
		
		<!-- JQueryUI Override -->
		<script src='jquery/jquery-ui/jquery-ui_override.js'></script>
		
		<!-- colResizable -->
		<script src='plugins/colResizable/colResizable-1.5.min.js'></script>
		
		<!-- tablesorter -->
		<script src='plugins/tablesorter/jquery.tablesorter.js'></script>
		<link rel='stylesheet' href='plugins/tablesorter/blue/style.css'>
		
		<!-- ms_window -->
		<script src='plugins/ms_window/ms_window.js'></script>
		<link rel='stylesheet' href='plugins/ms_window/ms_window.css'>
		
		<!-- ms_filetable -->
		<script> var ms_filetable_php='plugins/ms_filetable/ms_filetable.php' </script>
		<script src='plugins/ms_filetable/ms_filetable.js'></script>
		<link rel='stylesheet' href='plugins/ms_filetable/ms_filetable.css'>
	</head>
	
	<body>		
		<div id='1'></div>
		
	<script>
	
	//Alternate document.ready()
	$(function() {	
		var plugin = $('#1').ms_window();
		
		plugin.body().ms_filetable({directory: "/var/www/html/ms/data/user_data/cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e"});
	});
	</script>
	</body>
</html>