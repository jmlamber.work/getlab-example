(function($) {
	$.fn.ms_window = function() {
		var _this = this;
		
		
		this.addClass('ms_window');
		this.append(
			"<div class='ms_windowHeader'>" + 
				"<div class='ms_windowDragbar'>" + 
					"<div class='ms_windowDescriptor'></div>" + 
				"</div>" + 
				"<div class='ms_windowButtons'>" + 
					"<button class='ms_windowMax'> + </button>" + 
					"<button class='ms_windowMin'> - </button>" + 
					"<button class='ms_windowClose'> x </button>" + 
				"</div>" + 
			"</div>" + 
			"<div class='ms_windowBody'></div>"
		);
		
		this.resizable({
			handles: "all"
		});
		
		this.draggable({
			//Parent moves with drag bar
			handle: '.ms_windowDragbar'
		});
		
		//Returns the jquery object refering to the windows body
		this.body = function() {
			return $(_this.selector + " .ms_windowBody");
		};
		
		return this;
	};
}(jQuery));