//Filetable allows the user to browse shown directorys

(function($) {
	$.fn.ms_filetable = function(options) {
		var settings = $.extend(options);			
		var selectable; //our selectable plugin object
		var directory = settings.directory; //The currently displayed directory
		var _this = this; //For accessing within various scopes
		var column = [
			['name', 'Name', true],
			['type', 'Type', true],
			['time', 'Date Modified', true],
			['size', 'Size', true],
			['extension', 'Extension', false],
			['path', 'Path', false]
		]; //column[number][identifier, lable, active]
		
		// ---- functions ---- //
		//AJAX method for getting directory data
		function refresh(directory_i) {
			$.ajax({
				method: 'POST',
				url: ms_filetable_php,
				data: {directory: directory},
				dataType: 'json',
				success: function(data) {
					
					//Build table
					_this.populate(data);
				
					//Enable sorting
					$(_this.selector + " > table").tablesorter();
					
					//Enable selecting					
					selectable = $(_this.selector + "> table").ms_selectable();
					
					//Enable resizing
					$(_this.selector + " > table").colResizable({
						liveDrag: true
					});
					
					//Enable double-click
					$(_this.selector + " > table > tbody > tr").dblclick(function() {
						alert(selectable.selected()[0][0]);
					});
				}
			});
		} 

		//Quick method for accessing selected items
		this.getSelected = function() {
			return selected;
		}
		
		this.populate = function(data) {
			var table = "";
			
			//First, generate the columns
			var thead = "<thead class='ms_filetable'>" + "<tr class='ms_filetable'>"
			for(var i = 0; i < column.length; i++) {
				thead += "<th class='ms_filetable'>" + column[i][1] + '</th>';
			}
			thead += '</tr>' + '</thead>';
			
			
			//Populate our table with rows
			var tbody = "<tbody class='ms_filetable'>";
			for(var i = 0; i < data.length; i++) {
				var tr = "<tr class='ms_filetable'>";
				
				for(var j = 0; j < column.length; j++) {
					tr += "<td class='ms_filetable'>" + data[i][column[j][0]] + '</td>';
				}
				
				tr += '</tr>';
				tbody += tr;
			}
			tbody += '</tbody>';
			
			table = "<table class='ms_filetable'>" + thead + tbody + "</table>";
			
			//update the element with our new table
			$(_this.selector).html(table);
			
			//Hide inactive columns
			for(var i = 0; i < column.length; i++) {
				if(column[i][2] == false) {
					$(_this.selector + ' td:nth-child(' + (i+1) + '), ' + _this.selector + ' th:nth-child(' + (i+1) + ')').hide();
				}
				else {
					$(_this.selector + ' td:nth-child(' + (i+1) + '), ' + _this.selector + ' th:nth-child(' + (i+1) + ')').show();
				}
			}
		}
		
		//Set parent div type
		_this.addClass('ms_filetable_container');
		
		//Refresh (re-create/create) our table
		refresh(directory);

		return this;
	};
}(jQuery));