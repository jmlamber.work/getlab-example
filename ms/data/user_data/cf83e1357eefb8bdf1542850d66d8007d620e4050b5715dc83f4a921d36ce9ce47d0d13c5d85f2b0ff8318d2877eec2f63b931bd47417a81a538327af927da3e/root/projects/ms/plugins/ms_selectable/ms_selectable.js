//Selectable plugin ment for use in basic html tables

(function($) {	
	$.fn.ms_selectable = function() {
		var _this = this;
		var lastSelectedIndex = -1; //Last row selected
		var selected; //currently selected items
		
		$(this.selector + ' > tbody > tr').mousedown(function(event) {
			var selectedIndex = $(this).index();
			
			//shift-click (select all in range)
			if(event.shiftKey && lastSelectedIndex > -1) {
				var start = Math.min(selectedIndex, lastSelectedIndex);
				var end = Math.max(selectedIndex, lastSelectedIndex);
				
				//Note: nth-index starts at 1, not 0. Blame CSS.
				for(var i = start+1; i < end+2; i++) {
					$(_this.selector + '> tbody > tr:nth-child(' + i + ')').addClass('selected');
				}
			}
			
			//ctrl-click (select multiple)
			else if(event.ctrlKey) {
				$(this).addClass('selected');
			}
			
			
			//click (select only current)
			else {
				$(this).addClass('selected');
				$(this).siblings().removeClass('selected');
			}
			
			//Update our last selected index
			lastSelectedIndex = selectedIndex;
			
			//Update our list of selected items
			var selected_row = $(_this.selector + ' .selected');
			selected = [];
			
			for (var i = 0; i < selected_row.length; i++) {
				var row = {};
				for (var j = 0; j < selected_row[i].cells.length; j++)
					row[j] = selected_row[i].cells[j].innerHTML;
				selected.push(row);
			}
		});
		
		this.selected = function() {
			return selected;
		}
		
		return this;
	};
}(jQuery));