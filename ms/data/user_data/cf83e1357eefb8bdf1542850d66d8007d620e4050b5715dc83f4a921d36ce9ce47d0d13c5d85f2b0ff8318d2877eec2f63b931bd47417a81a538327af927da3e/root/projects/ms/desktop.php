<html>
	<head>
		
		<!-- JQuery -->
		<script src="jquery/jquery-2.1.3.js"></script>
		
		<!-- JQueryUI -->
		<script src="jquery/jquery-ui/jquery-ui.min.js"></script>
		<link rel='stylesheet' href='jquery/jquery-ui/jquery-ui.min.css'>
		
		<!-- JQueryUI Override -->
		<script src='jquery/jquery-ui/jquery-ui_override.js'></script>
		
		<!-- colResizable -->
		<script src='plugins/colResizable/colResizable-1.5.min.js'></script>
		
		<!-- tablesorter -->
		<script src='plugins/tablesorter/jquery.tablesorter.js'></script>
		<link rel='stylesheet' href='plugins/tablesorter/blue/style.css'>
		
		<!-- ms_window -->
		<script src='plugins/ms_window/ms_window.js'></script>
		<link rel='stylesheet' href='plugins/ms_window/ms_window.css'>
		
		<!-- ms_filetable -->
		<script> var ms_filetable_php='plugins/ms_filetable/ms_filetable.php' </script>
		<script src='plugins/ms_filetable/ms_filetable.js'></script>
		<link rel='stylesheet' href='plugins/ms_filetable/ms_filetable.css'>

		<!-- ms_selectable -->
		<script src='plugins/ms_selectable/ms_selectable.js'></script>
	
	<style>	
		body {
			padding: 0px;
			margin: 0px;
		}
	
		#desktop {
			width: 100%;
			height: 100%;
			overflow: hidden;
			padding: 0px;
			margin: 0px;
			background-image: url('wallpaper.jpg');
			background-size: 100% calc(100% - 40px);
		}
		
		#taskbar {
			position: fixed;
			bottom: 0px;
			left: 0px;
			width: 100%;
			height: 40px;
			background-color: yellow;
		}
	</style>
	</head>
	
	<body>
		<div id='desktop'>
			<div id='taskbar'></div>
		</div>
		
		<script>
		var taskbar = []; //multidemension array holding all similar app-types in a single position
		var idCounter = 0;
			
			//generate the app requested (For this version, just the file browser)
			function createApp(type) {
				if(type == 'ms_filetable') {
					
					//Create app container
					var ele = document.createElement('div');
					ele.id = idCounter;
					$('#desktop').append(ele);
					
					//Initialize window
					var plugin = $('#' + idCounter).ms_window();
					
					//Initialize content
					plugin = plugin.body().ms_filetable({directory: "/var/www/html/ms/data/user_data/cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e"});
					
					//Search and add to our taskbar
					addToTaskbar(plugin, type);
				}
			};
			
			function addToTaskbar(app, type) {
				
			}
			
			createApp('ms_filetable');
			
		</script>
	</body>

</html>