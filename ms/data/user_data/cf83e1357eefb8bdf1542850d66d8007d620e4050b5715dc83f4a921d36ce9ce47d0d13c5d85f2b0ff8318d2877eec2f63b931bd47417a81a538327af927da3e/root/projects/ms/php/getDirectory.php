<?php
	session_start();
	
	function formatSizeUnits($bytes) {
        if ($bytes >= 1073741824)
            $bytes = number_format($bytes / 1073741824, 2) . 'GB';
        elseif ($bytes >= 1048576)
            $bytes = number_format($bytes / 1048576, 2) . 'MB';
        elseif ($bytes >= 1024)
            $bytes = number_format($bytes / 1024, 2) . 'KB';
        elseif ($bytes > 1)
            $bytes = $bytes . 'B';
        else
            $bytes = '0B';
        return $bytes;
	}

	$dir = $_SESSION['currentDirectory'];
	$files = scandir($dir);
	
	//Build our result array
	$result = array();
	for ($i = 0; $i < count($files); $i++) {
		$fileInfo = pathinfo($dir . '/' . $files[$i]);
		
		$result[$i]['name'] = $files[$i];
		$result[$i]['type'] = filetype($dir . '/' . $files[$i]);
		$result[$i]['time'] = date("m/d/y H:m", filemtime($dir . '/' . $files[$i]));
		$result[$i]['size'] = formatSizeUnits(filesize($dir . '/' . $files[$i]));
		
		$result[$i]['extension'] = $fileInfo['extension'];
		$result[$i]['path'] = $dir . '/' . $files[$i];
	}
	
	//Remove current directory
	array_shift($result);
	
	//If at base directory, remove parent directory
	if($_SESSION['currentDirectory'] == $_SESSION['baseDirectory'])
		array_shift($result);
	
	//For easy viewing, sort it by directory, then by additional sort methods
	//Default should be alpha decending
	
	echo json_encode($result);
?>