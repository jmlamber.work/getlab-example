<html>
	<head>
		<!-- JQuery -->
		<script src="js/jquery/jquery-2.1.3.js"></script>
		
		<!-- JQueryUI -->
		<script src="js/jquery/jquery-ui/jquery-ui.min.js"></script>
		<link rel='stylesheet' href='js/jquery/jquery-ui/jquery-ui.min.css'>
		
		<!-- ms_window -->
		<script src='js/ms_window.js'></script>
		<link rel='stylesheet' href='css/ms_window.css'>
		
		<!-- CSS -->
		
		<link rel='stylesheet' href='css/ms_jquery-ui.css'>
	</head>
	
	<body>
		<div id='1'> </div>
		
		<script>
			$(function() {				
				var plugin = $('#1').ms_window();
			});
		
		</script>
	</body>
</html>