<?php

?>

<html>
<head>

	<!-- Font Awesome -->
	<link rel='stylesheet' href='css/font-awesome-4.5.0/css/font-awesome.css'>

	<!-- JQuery -->
	<script src="jquery/jquery-2.1.3.js"></script>
	
	<!-- ms_mouse -->
	<script src='js/ms_mouse.js'></script>
	
	<!-- ms_contextmenu -->
	<script src='plugins/ms_contextmenu/ms_contextmenu.js'></script>
	<link rel='stylesheet' href='plugins/ms_contextmenu/ms_contextmenu.css'>
	
	<style>
		body {
			margin: 0;
			padding: 0;
		}
	
		#contextBox {
			width: 100%;
			height: 50%;
			background-color: lightgray;
			border-radius: 3px;
			margin: 0;
			padding: 0;
		}
		
		#contextBox2 {
			width: 100%;
			height: 50%;
			background-color: lightyellow;
		}
	</style>
</head>
<body>
	<div id='contextBox'></div>
	
	<div id='contextBox2'></div>
	
	<script>
		var menu;
		var menuState = 0;
	
		$('#contextBox').ms_contextmenu({
			list: generate_list,
			context: this
		});
		
		$('#contextBox2').ms_contextmenu({
			list: generate_list_again,
			context: this
		})
		
		function generate_list_again() {
			var list = [];
			var catagory = [];
			catagory.push({
				text: 'working?',
				hint: 'please?'
			});
			list.push(catagory);
			
			return list;
		}
		
		function generate_list() {
			var submenuCatagories = [];
			var submenuItems = [];
			submenuItems.push({
				text: 'subitem 1',
				hint: 'subitem here!'
			});
			submenuItems.push({
				text: 'subitem 2'
			});
			submenuCatagories.push(submenuItems);
			
			var catagories = [];
			var items = [];
			items.push({
				text: 'item 1',
				hint: 'ctrl+NA',
				call: greet
			});
			
			items.push({
				text: 'item 2',
				hint: 'NA'
			});
			
			catagories.push(items);
			items = [];
			
			items.push({
				text: 'item 3b', 
				hint: 'nope, none.',
				submenu: submenuCatagories
			});
			
			catagories.push(items);
			
			return catagories;
		}
		
		function greet() {
			console.log('hello you!');
		}
	
	</script>
</body>
</html>