<?php
	session_start();
	
	$files = json_decode($_POST['clipboard'], true);
	$method = $_POST['clipboardState'];
	
	//Due to a flaw in JSON, we're required to use numbered indexes. Yay.
	$path = 0;
	$name = 1;
	
	if($method == 'copy') {
		for($i = 0; $i < count($files); $i++) {
			copy($files[$i][$path], $_SESSION['currentDirectory'] . '/' . $files[$i][$name]);
		}
	}

	else if($method == 'cut') {
		for($i = 0; $i < count($files); $i++) {
			rename($files[$i][$path], $_SESSION['currentDirectory'] . '/' . $files[$i][$name]);
		}
	}

?>