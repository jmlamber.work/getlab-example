<?php

?>

<html>
<head>
	<style>
		body {
			width: 100%;
			height: 100%;
			margin: 0;
			padding: 0;
		}
		
		.filedrag {
			position: absolute;
			width: 300px;
			height: 200px;
			top: calc(45% - 100px);
			left: calc(50% - 150px);
			background-color: lightgray;
			border: 1px solid black;
			border-radius: 5px;
		}
		
		.filedrag:hover {
			background-color: lightblue;
		}
	</style>
</head>
<body>
	<div id='filedrag' class='filedrag'> </div>
	
	<script>
	
		if (!window.File || !window.FileList || !window.FileReader) {
			console.log('WARNING: Drag/drop upload may not work!');
		}
		else
			init();
		
		function init() {
			var filedrag = document.getElementById('filedrag');
			
			// is XHR2 available?
			var xhr = new XMLHttpRequest();
			if (xhr.upload) {

				// file drop
				filedrag.addEventListener("dragover", FileDragHover, false);
				filedrag.addEventListener("dragleave", FileDragHover, false);
				filedrag.addEventListener("drop", FileSelectHandler, false);
				filedrag.style.display = "block";
			}
		}
		
		function FileDragHover(e) {
			e.preventDefault();
		}

		function FileSelectHandler(e) {
			
			FileDragHover(e);
			
			// fetch FileList object
			var files = e.target.files || e.dataTransfer.files;

			// process all File objects
			for (var i = 0, f; f = files[i]; i++) {
				ParseFile(f);
			}	
			
		}
		
		function ParseFile(file) {
			console.log(file.name + ' | ' + file.type + ' | ' + file.size);
		}
		
	</script>
</body>
</html>