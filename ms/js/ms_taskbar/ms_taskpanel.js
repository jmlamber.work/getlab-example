//*** TASKBAR TASKPANEL ***//
var taskbar = []; //multidemension array holding all similar app-types in a single position

function initTaskbar() {

	//Load taskbar settings (pinned items)
	
	//Refresh
	refreshTaskbar();
}

function taskpanel_contextmenu() {
	var list = [];
	var catagory = [];
	
	catagory.push(
		{
			text: 'Maximize All',
			call: maximize_all
		},
		{
			text: 'Minimize All',
			call: minimize_all
		},
		{
			text: 'Bring All To Front',
			call: bring_to_front
		}
	);
	
	list.push(catagory);
	return list;
}

//Sort through windows, setting all to max
function maximize_all() {
	
}


//Sort through windows, setting all to min
function minimize_all() {
	
}

//Sort through windows, putting all at the top-left of the screen
function bring_to_front() {
	
}

function addToTaskbar(app, pluginName) {
	
	//search for similar type apps
	var i;
	var found;
	for(i = 0; i < taskbar.length; i++) {
		if(taskbar[i].pluginName == pluginName) {
			found = true;
			break;
		}
	}
	
	//If found but not active, generate new slot
	if(found && taskbar[i].pinned) {
		taskbar[i].apps.push(app);
	}
	
	//If found and already active, just add to slot
	else if(found && taskbar[i].apps.length >= 1) {
		taskbar[i].apps.push(app);
	}
	
	//If not, generate new slot
	else {
		var slot = {
			pluginName: pluginName,
			pinned: false,
			apps: []
		}
		
		taskbar.push(slot);
		taskbar[i].apps.push(app);
	}
	
	refreshTaskbar();
}

function refreshTaskbar() {
	
	for(var i = 0; i < taskbar.length; i++) {
		var pinned = taskbar[i].pinned;
		var pluginName = taskbar[i].pluginName;
		var apps = taskbar[i].apps;
		
		//Clear the taskpanel of icons
		$('#ms_taskbar_taskpanel').empty();
		
		//generate our new taskbar icon
		var ele = generateTaskbarIcon(taskbar[i]);
		$('#ms_taskbar_taskpanel').append(ele);	
		
		//If pinned, connect to create-app		
		if(pinned && apps.length == 0) {
			$(ele).click(function() {
				createApp(pluginName);
			});
		}
		
		//If alone, send this element to the plugin
		else if(apps.length == 1)		
			taskbar[i].apps[0].getWindow().taskbar(ele, false);
		
		//If multiple open, generate and send sub-elements to each app
		else if(apps.length > 1) {
			generateTaskpanelSubmenu(ele, taskbar[i]);
		}

	}
}

function generateTaskpanelSubmenu(ele, taskbarItem) {
	var appCount = taskbarItem.apps.length;
	var itemHeight = 30;
	var height = appCount * itemHeight;
	
	var submenu = document.createElement('div');
	$(submenu).addClass('ms_taskbar_taskpanelSubmenu');
	$(submenu).css({'height':height, 'bottom':height+5});
	
	//For each app, add a new item to the submenu
	for(var i = 0; i < taskbarItem.apps.length; i++) {
		
		//create our submenu item
		var item = document.createElement('div');
		$(item).addClass('ms_taskbar_taskpanelSubmenuItem');
		
		//Its descriptor
		var desc = document.createElement('div');
		var html = taskbarItem.apps[i].getWindow().getHeader();
		$(desc).addClass('ms_taskbar_taskpanelSubmenuItemDescriptor');
		$(desc).html(html);
		$(item).append(desc);
		
		//Its button
		var button = document.createElement('button');
		$(button).addClass('ms_taskbar_taskpanelSubmenuItemButton');
		$(button).text('x');
		$(item).append(button);
		
		$(submenu).append(item);
		taskbarItem.apps[i].getWindow().taskbar(item, true);
	}
	
	//hide the submenu depending on where we've clicked
	$(ele).click(function(e) {
		$(submenu).show();
		
		$(ele).on('click.this', function(e) {
			$(submenu).hide();
			
			//remove unnecessary events
			$(ele).off('click.this');
			$(ele).off('mousedown.this');
			$(document).off('mousedown.taskbarTaskpanelSubmenuIcon');
			taskbarTaskpanelSubmenuMousedown = false;
		});
		
		$(ele).on('mousedown.this', function() {
			taskbarTaskpanelSubmenuMousedown = !taskbarTaskpanelSubmenuMousedown;
		});
		
		$(document).on('mousedown.taskbarTaskpanelSubmenuIcon', function() {
			if(!taskbarTaskpanelSubmenuMousedown) {
				$(submenu).hide();
				
				//remove unnecessary events
				$(ele).off('click.this');
				$(ele).off('mousedown.this');
				$(document).off('mousedown.taskbarTaskpanelSubmenuIcon');
			}
			else
				taskbarTaskpanelSubmenuMousedown = false;
		});	
	});
	
	$(ele).append(submenu);
}

//Generates an application taskbar icon depending on the number of apps open, whether the app is active or not, etc.
function generateTaskbarIcon(appSlot) {
	var appCount = appSlot.apps.length;
	
	var container = document.createElement('div');
	var icon;
	var tabs;
	
	//Depending on the number of similar apps open, generate different icon containers
	//Pinned in this case	
	if(appCount == 0)
		container.className += ' ms_taskbar_taskpanelIconPinned';
	
	else if(appCount == 1) {
		container.className += ' ms_taskbar_taskpanelIcon1Container';
		
		icon = document.createElement('div');
		icon.className += ' ms_taskbar_taskpanelIcon1Icon';
		container.appendChild(icon);
	}
	
	else if(appCount == 2) {
		container.className += ' ms_taskbar_taskpanelIcon2Container';
		
		tabs = document.createElement('div');
		tabs.className += ' ms_taskbar_taskpanelIcon2Tabs';
		container.appendChild(tabs);
		
		icon = document.createElement('div');
		icon.className += ' ms_taskbar_taskpanelIcon2Icon';
		container.appendChild(icon);
	}
	
	else if(appCount >= 3) {
		container.className += ' ms_taskbar_taskpanelIcon3Container';
		
		tabs = document.createElement('div');
		tabs.className += ' ms_taskbar_taskpanelIcon3Tabs';
		container.appendChild(tabs);
		
		icon = document.createElement('div');
		icon.className += ' ms_taskbar_taskpanelIcon3Icon';
		container.appendChild(icon);
	}
	
	//Finally, add the app-specific icon
	if(appSlot.pluginName == 'ms_filetable') {
		if(appCount == 0)
			container.className += ' ms_filetable_icon';
		else
			icon.className += ' ms_filetable_icon';
	}
	
	return container;
}