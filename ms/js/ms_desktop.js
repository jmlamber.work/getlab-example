//NOTE: The jobject.selector attribute is, while effective, DANGEROUS to use without a set guideline. That is because rather than using some value which SHOULD identify the jobject, it simply uses what WAS used in the case that you actually input a string selector. Using a html element or other variable as a selector will cause the selector attribute to have the value ''. Depending on how dependant the program is on selectors, changing just one could have a catastrophic effect (we were lucky to identify the problem this time!). As such, from here on we should use jobject.attr('id') as a method of specifying the object in use.

//NOTE: In the case of an event not propogating to the beginning, many plugins may (for whatever reason) BLOCK this feature. To be more specific, returning false from an event handler will prevent both default behavior AND event propagation, while using e.preventDefaultBehavior() will only prevent the default behavior.


function init() {
	//setup our clock
	updateTime();

	//Initialize taskbar
	initTaskbar();
	
	//Initialize taskbar start menu
	ms_startmenu_init();
}


//*** APP CREATION/DESTRUCTION ***//
//generate the app requested (For this version, just the file browser)
function createApp(pluginName, appOptions) {
	var ele;
	var window;
	var app;
	
	//Create app container
	ele = document.createElement('div');
	$('#desktop').append(ele);
	
	//Initialize window
	window = $(ele).ms_window({
		id: idCounter
	});
	
	app = window.body()[pluginName]({
		id: idCounter,
		appOptions: appOptions
	});
			
	//connect the window and app function wise
	window.setApp(app);
	app.setWindow(window);
	
	//Initialize content
	app.init();
	
	//Search and add to our taskbar
	addToTaskbar(app, pluginName);
	
	//Increment counter for the next app
	idCounter += 1;
};

//Destroy an app completely
function killApp(app) {	
	var appID = app.getID();
	
	//Find our app in the taskbar and remove it
	for(var i = 0; i < taskbar.length; i++) {
		for(var j = 0; j < taskbar[i].apps.length; j++) {
			if(taskbar[i].apps[j].getID() == appID) {
				taskbar[i].apps.splice(j, 1);
			}
		}
	}
	
	//destroy the window and its children
	app.parent().remove();
	
	//Refresh our taskbar to remove unneeded icons
	refreshTaskbar();
}


